import { IPokemon } from './../models/pokemon.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ITrainer } from '../models/trainer.model';

@Injectable({
  providedIn: 'root',
})
export class TrainerApiService {
  private API_BASE_URL: string =
    'https://noit-noroff-assignment-api.herokuapp.com';
  private API_KEY: string = '105E23DD288A4ED9B869F385818AE928';

  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  // trainer param: the trainer to update
  // pokemon param: the pokemon to add to trainer
  catchPokemon(trainer: ITrainer | null, pokemon: IPokemon) {
    if (trainer !== null) {
      // add pokemon to trainers pokemon array
      trainer.pokemon.push(pokemon);
      this.http
        .patch(
          `${this.API_BASE_URL}/trainers/${trainer.id}`,
          { pokemon: trainer.pokemon }, // body that gives "pokemon" in the api the new pokemon array
          {
            headers: {
              'X-Api-Key': this.API_KEY,
              'Content-Type': 'application/json',
            },
          }
        )
        .subscribe({
          next: (patchResponse) => {
            console.info('Updated trainer pokemons', patchResponse);
          },
          error: (error: HttpErrorResponse) => {
            console.error('Patch error:', error);
            this._error = error.message;
          },
        });
    }
  }
  releasePokemon(trainer: ITrainer, pokemonToRelease: IPokemon) {
    if (trainer !== null) {
      // remove pokemon to trainers pokemon array
      for (const trainerPokemon of trainer.pokemon) {
        if (trainerPokemon.name === pokemonToRelease.name) {
          const indexOfPokemonToRelease =
            trainer.pokemon.indexOf(pokemonToRelease);
          trainer.pokemon.splice(indexOfPokemonToRelease, 1); // remove the pokemon from trainer.pokemon array.
        }
      }
      // update api data with removed pokemon
      this.http
        .patch(
          `${this.API_BASE_URL}/trainers/${trainer.id}`, // url
          { pokemon: trainer.pokemon }, // body of patch request
          {
            headers: {
              'X-Api-Key': this.API_KEY,
              'Content-Type': 'application/json',
            },
          }
        )
        .subscribe({
          next: (patchResponse) => {
            console.info('Updated trainer pokemons', patchResponse);
          },
          error: (error: HttpErrorResponse) => {
            console.error('Patch error:', error);
            this._error = error.message;
          },
        });
    }
  }
}
