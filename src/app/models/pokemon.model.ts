export interface IPokemonCatalogue {
    //TODO: Create pokemon model
    count: number
    next: string
    previous: string
    results: IPokemon[]

}

export interface IPokemon {
    name: string
    url: string
    image: string
}