// 3rd party imports
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap, tap } from 'rxjs';


// first party imports
import { StorageKeys } from '../enums/storage-keys.enum';
import { StorageUtil } from '../utils/storage.util';
import { ITrainer } from './../models/trainer.model';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private API_BASE_URL: string =
    'https://noit-noroff-assignment-api.herokuapp.com';
  private API_KEY: string = '105E23DD288A4ED9B869F385818AE928';
  private _loggedInTrainer: ITrainer | null = null;
  private _error: string = ''

  constructor(private readonly http: HttpClient) { }

  public loginWithTrainerName(trainerName: string): Observable<ITrainer> {
    return this.checkTrainerName(trainerName)
      .pipe(
        switchMap((trainer: ITrainer | undefined) => {
          if (trainer === undefined) {
            return this.createTrainer(trainerName)
          }
          return of(trainer)
        }),
        tap((trainer: ITrainer) => {
          StorageUtil.storageSave<ITrainer>(StorageKeys.Trainer, trainer);

          // Sets loged in trainer.
          this._loggedInTrainer = StorageUtil.storageRead(StorageKeys.Trainer)

        })
      )
  }

  private checkTrainerName(TrainerName: string): Observable<ITrainer | undefined> {
    return this.http.get<ITrainer[]>(`${this.API_BASE_URL}/trainers?username=${TrainerName}`)
      .pipe(
        map((response: ITrainer[]) => response.pop())
      )
  }

  private createTrainer(trainerName: string): Observable<ITrainer> {
    const trainer = {
      id: 0,
      username: trainerName,
      pokemon: [],
    };

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "X-Api-Key": this.API_KEY
    })

    return this.http.post<ITrainer>(`${this.API_BASE_URL}/trainers`, trainer, {
      headers
    })
  }

  get loggedInTrainer(): ITrainer | null {
    return this._loggedInTrainer;
  }
}
