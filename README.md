# Assignment4

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.2.
# Preview

A screenshot of the running app: 
![image.png](./image.png)

# Installation / Link to running site

The app running can be seen here: https://allancarlsen.gitlab.io/assignment-4/

If you want to run it locally you do the following:

1. clone this repository
2. run npm install in the directory you installed it in


# Usage

Below you will find how to run the webapp, in addtion to other Angular commands.

## Dev server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
