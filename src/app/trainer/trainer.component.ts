import { IPokemon } from './../models/pokemon.model';
import { LoginService } from './../services/login.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ITrainer } from '../models/trainer.model';
import { TrainerApiService } from '../services/trainer-api.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css'],
})
export class TrainerComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly trainerApiService: TrainerApiService,
    private readonly loginService: LoginService
  ) {}

  ngOnInit(): void {
    // if not logged in - redirect to landing
    if (this.trainer === null) {
      console.info('Redirecting to login from trainer page');
      this.router.navigateByUrl('/');
    } else {
      console.log('Current trainer:', this.trainer);
    }
  }
  public releasePokemon(trainer: ITrainer, pokemon: IPokemon) {
    // release pokemon from trainer
    console.log("released: "+pokemon.name);
    this.trainerApiService.releasePokemon(trainer, pokemon);
  }
  get trainer(): ITrainer | null {
    return this.loginService.loggedInTrainer;
  }
}
