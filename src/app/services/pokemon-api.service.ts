import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPokemon, IPokemonCatalogue } from '../models/pokemon.model';



@Injectable({
  providedIn: 'root'
})
export class PokemonApiService {

  private _pokemonCatalogue: IPokemonCatalogue = {
    count: 0,
    next: '',
    previous: '',
    results: []
  };
  private _pokemons: IPokemon[] = [];
  private _error: string = '';

  private BASE_IMG_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"

  constructor(private readonly http: HttpClient) {
  }

  fetchPokemons(): void {
    this.http.get<IPokemonCatalogue>('https://pokeapi.co/api/v2/pokemon?limit=100')
      .subscribe({
        next: (pokemonCatalogue: IPokemonCatalogue) => {

          // OBS: Might be ineffcient to set both _pokemonCatalogue and _pokemons
          this._pokemonCatalogue = pokemonCatalogue;
          this._pokemons = pokemonCatalogue.results;

          // Extracts the pokemon id from url and adds a path to pokemon-image
          this._pokemons.forEach(pokemon => {
            let id = pokemon.url.split("/")
            pokemon.image = this.BASE_IMG_URL + id[6] + ".png"
          });
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      });
  }




  public pokemonCatalogue(): IPokemonCatalogue {
    return this._pokemonCatalogue;
  }

  public pokemons(): IPokemon[] {
    return this._pokemons;
  }

  public erros(): string {
    return this._error;
  }

}
