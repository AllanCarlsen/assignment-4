import { IPokemon } from './pokemon.model';
export interface ITrainer {
  id: number;
  username: string;
  pokemon: IPokemon[];
}