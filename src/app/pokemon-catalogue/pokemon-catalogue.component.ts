// 3rd party imports
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

// 1st party imports
import { LoginService } from './../services/login.service';
import { ITrainer } from './../models/trainer.model';
import { IPokemon, IPokemonCatalogue } from '../models/pokemon.model';
import { PokemonApiService } from '../services/pokemon-api.service';
import { TrainerApiService } from './../services/trainer-api.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css'],
})
export class PokemonCatalogueComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly pokemonApiService: PokemonApiService,
    private readonly trainerApi: TrainerApiService,
    private readonly loginService: LoginService
  ) {}

  ngOnInit(): void {
    // if not logged in - redirect to landing
    if (this.loginService.loggedInTrainer === null) {
      this.router.navigateByUrl('/');
    } else {
      // Fetches pokemons on init
      this.pokemonApiService.fetchPokemons();
    }
  }

  // Gets the pokemonCatalogue for potential paging
  get pokemonCatalogue(): IPokemonCatalogue {
    return this.pokemonApiService.pokemonCatalogue();
  }

  // Only gets the pokemons
  get pokemons(): IPokemon[] {
    return this.pokemonApiService.pokemons();
  }
  // get logged in trainer
  get trainer(): ITrainer | null {
    return this.loginService.loggedInTrainer;
  }
  // checks if the logged in trainer owns the pokemon from parameter
  trainerOwnsPokemon(pokemon: IPokemon): Boolean | undefined {
    const isOwned = this.trainer?.pokemon.some(
      (trainerPokemon) => {
        return trainerPokemon.name === pokemon.name;
      }
    );
    return isOwned;
  }

  onPokemonClicked(pokemon: IPokemon): void {
    if (this.trainerOwnsPokemon(pokemon)) {
      console.log('Trainer already has', pokemon);
      return;
    }

    this.trainerApi.catchPokemon(this.loginService.loggedInTrainer, pokemon);
  }
}
