// 3rd party import
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

// Custom imports
import { AppComponent } from './app.component';
import { AppRouterModule } from './app-router/app-router.module';
import { TrainerComponent } from './trainer/trainer.component';
import { LandingComponent } from './landing/landing.component';
import { PokemonCatalogueComponent } from './pokemon-catalogue/pokemon-catalogue.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TrainerComponent,
    LandingComponent,
    PokemonCatalogueComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRouterModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
