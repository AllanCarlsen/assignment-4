import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ITrainer } from '../models/trainer.model';

import { LoginService } from './../services/login.service';
import { TrainerApiService } from './../services/trainer-api.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
})
export class LandingComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly trainerApi: TrainerApiService,
    private readonly loginService: LoginService
  ) { }

  ngOnInit(): void {
    if (this.loginService.loggedInTrainer !== null) {
      this.router.navigateByUrl('pokemon-catalogue');
    }
  }

  public loginAsTrainer(loginForm: NgForm): void {
    const { name } = loginForm.value;

    this.loginService.loginWithTrainerName(name)
      .subscribe({
        next: (trainer: ITrainer) => {
          console.log(`You are logged in as: ${this.loginService.loggedInTrainer?.username}`);
          this.router.navigateByUrl('/pokemon-catalogue')
        },
        error: () => {
          
        }
      })
  }
}
