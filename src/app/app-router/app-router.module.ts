import { TrainerComponent } from './../trainer/trainer.component';
import { PokemonCatalogueComponent } from './../pokemon-catalogue/pokemon-catalogue.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from '../landing/landing.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full', // prefix / full are the two options available. prefix is default behaviour
    component: LandingComponent,
  },
  {
    path: 'pokemon-catalogue',
    component: PokemonCatalogueComponent
  },
  {
    path: 'trainer',
    component: TrainerComponent
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule] // when you update a module in imports you export it here. 
})
export class AppRouterModule {}
